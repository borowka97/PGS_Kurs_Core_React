﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Pgs.Kanban.Domain.Dtos
{
    public class EditListNameDto
    {
        public string Name { get; set; }
        public int ListId { get; set; }
        public int BoardId { get; set; }
    }
}
