﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Pgs.Kanban.Domain.Migrations
{
    public partial class Card123 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lists_Lists_ListId",
                table: "Lists");

            migrationBuilder.DropIndex(
                name: "IX_Lists_ListId",
                table: "Lists");

            migrationBuilder.DropColumn(
                name: "ListId",
                table: "Lists");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ListId",
                table: "Lists",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lists_ListId",
                table: "Lists",
                column: "ListId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lists_Lists_ListId",
                table: "Lists",
                column: "ListId",
                principalTable: "Lists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
