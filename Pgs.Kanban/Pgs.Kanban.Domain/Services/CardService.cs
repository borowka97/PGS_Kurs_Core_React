﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Pgs.Kanban.Domain.Dtos;
using Pgs.Kanban.Domain.Models;

namespace Pgs.Kanban.Domain.Services
{
    public class CardService
    {
        private readonly KanbanContext _context;

        public CardService()
        {
            _context = new KanbanContext();
        }


        public CardDto CreateCard(CreateCardDto createCardDto)
        {
            var card = new Card()
            {
                Name = createCardDto.Name
            };

            _context.Cards.Add(card);
            _context.SaveChanges();

            var cardDto = new CardDto()
            {
                Id = card.Id,
                Name = card.Name,
                ListId = card.ListId
            };

            return cardDto;
        }
    }
}
